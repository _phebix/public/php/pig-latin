Pig Latin Translator
====================

Simple Pig Latin translator for PHP


Instalation
-----------

Add this repo into your composer.json
```
"repositories": [
	{
		"type": "vcs",
		"url":  "git@gitlab.com:_phebix/public/php/pig-latin.git"
	}
]
```

Install it using Composer
```
composer require phebix/pig-latin
```

Usage
-----

Base usage:
```php
$translator = new \Phebix\PigLatinTranslator();
 
// Translate text
echo $translator->translate($text);
```

Advantage usage:
```php
$translator = new \Phebix\PigLatinTranslator();

// optional config for use hyphen in words
$translator->setUseHyphen(true);
 
// Optional set vowel suffix to 'way'
$translator->setVowelSuffix($translator::VOWEL_SUFFIX_WAY);
 
// Optional set consonant suffix to 'a'
$translator->setConsonantSuffix($translator::CONSONANT_SUFFIX_A);

// Translate text
echo $translator->translate($text);
```

Available vowel suffix:
```php
\Phebix\PigLatinTranslator::VOWEL_SUFFIX_AY - default option
\Phebix\PigLatinTranslator::VOWEL_SUFFIX_YAY
\Phebix\PigLatinTranslator::VOWEL_SUFFIX_WAY
\Phebix\PigLatinTranslator::VOWEL_SUFFIX_HAY
```

Available consonant suffix:
```php
\Phebix\PigLatinTranslator::CONSONANT_SUFFIX_A - default option
\Phebix\PigLatinTranslator::CONSONANT_SUFFIX_AY
```
