<?php
/**
 * BaseTest
 *
 * User: phebix
 * Date: 12.9.17
 * Time: 7:48
 */

namespace PigLatinTests;

require_once __DIR__ . '/bootstrap.php';

use Tester\Assert;
use Tester\TestCase;

class BaseTest extends TestCase
{

    /**
     * Test parameters
     */
    public function testParams()
    {

        $translator = new \Phebix\PigLatinTranslator();

        Assert::noError(function () use ($translator) {
            $translator->setUseHyphen(true);
        });

        Assert::noError(function () use ($translator) {
            $translator->setVowelSuffix($translator::VOWEL_SUFFIX_AY);
        });

        Assert::noError(function () use ($translator) {
            $translator->setVowelSuffix($translator::VOWEL_SUFFIX_WAY);
        });

        Assert::noError(function () use ($translator) {
            $translator->setVowelSuffix($translator::VOWEL_SUFFIX_YAY);
        });

        Assert::noError(function () use ($translator) {
            $translator->setVowelSuffix($translator::VOWEL_SUFFIX_HAY);
        });

        Assert::noError(function () use ($translator) {
            $translator->setConsonantSuffix($translator::CONSONANT_SUFFIX_A);
        });

        Assert::noError(function () use ($translator) {
            $translator->setConsonantSuffix($translator::CONSONANT_SUFFIX_AY);
        });

        Assert::exception(function () use ($translator) {
            $translator->setVowelSuffix('z');
        }, \InvalidArgumentException::class);

        Assert::exception(function () use ($translator) {
            $translator->setConsonantSuffix('z');
        }, \InvalidArgumentException::class);

        Assert::error(function () use ($translator) {
            $translator->translate(['a', 'b']);
        }, \TypeError::class);

        Assert::noError(function () use ($translator) {
            $translator->translate('a');
        });

    }

    //=========================================================================
    /**
     * Test translate simple English words to Pig latin
     */
    public function testSimple()
    {

        $translator = new \Phebix\PigLatinTranslator();

        $list = require __DIR__ . '/files/simpleList.php';

        foreach ($list as $en => $pl) {
            Assert::same($pl, $translator->translate($en));
        }

    }

    //=========================================================================
    /**
     * Test translate 'eagle' English words to Pig latin
     */
    public function testEagleVariants()
    {

        $en = 'eagle';

        $translator = new \Phebix\PigLatinTranslator();

        $translator->setUseHyphen(true);


        $translator->setVowelSuffix($translator::VOWEL_SUFFIX_AY);
        Assert::same('eagle-ay', $translator->translate($en));

        $translator->setVowelSuffix($translator::VOWEL_SUFFIX_YAY);
        Assert::same('eagle-yay', $translator->translate($en));

        $translator->setVowelSuffix($translator::VOWEL_SUFFIX_WAY);
        Assert::same('eagle-way', $translator->translate($en));

        $translator->setVowelSuffix($translator::VOWEL_SUFFIX_HAY);
        Assert::same('eagle-hay', $translator->translate($en));

    }

    //=========================================================================
    /**
     * Test translate simple English words to Pig latin with hyphen
     */
    public function testSimpleWithHyphen()
    {

        $translator = new \Phebix\PigLatinTranslator([
            'useHyphen' => true,
        ]);

        $list = require __DIR__ . '/files/simpleListHyphen.php';

        foreach ($list as $en => $pl) {
            Assert::same($pl, $translator->translate($en));
        }

    }

    //=========================================================================
    /**
     * Test translate simple English words to Pig latin
     *
     * Words source from https://en.wikipedia.org/wiki/Pig_Latin
     */
    public function testExtend()
    {

        $translator = new \Phebix\PigLatinTranslator([
            'vowelSuffix'     => \Phebix\PigLatinTranslator::VOWEL_SUFFIX_WAY,
            'consonantSuffix' => \Phebix\PigLatinTranslator::CONSONANT_SUFFIX_A,
        ]);

        $list = require __DIR__ . '/files/extendList.php';

        foreach ($list as $en => $pl) {
            Assert::same($pl, $translator->translate($en));
        }

    }

    //=========================================================================
    /**
     * Paragraph of Lorem ipsum text, test convert from http://www.snowcrest.net/donnelly/piglatin.html
     */
    public function testLorem()
    {

        $translator = new \Phebix\PigLatinTranslator([
            'vowelSuffix' => \Phebix\PigLatinTranslator::VOWEL_SUFFIX_WAY,
        ]);

        $en = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
        $pl = 'Oremlay ipsumway olorday itsay ametway, onsecteturcay adipiscingway elitway, edsay oday eiusmodway emportay incididuntway utway aborelay etway oloreday agnamay aliquaway. Utway enimway adway inimmay eniamvay, isquay ostrudnay exercitationway ullamcoway aborislay isinay utway aliquipway exway eaway ommodocay onsequatcay. Uisday auteway irureway olorday inway eprehenderitray inway oluptatevay elitvay esseway illumcay oloreday euway ugiatfay ullanay ariaturpay. Excepteurway intsay occaecatway upidatatcay onnay oidentpray, untsay inway ulpacay iquay officiaway eseruntday ollitmay animway idway estway aborumlay.';

        Assert::same($pl, $translator->translate($en));
    }

    //=========================================================================

}

(new BaseTest)->run();