<?php
return [
    'banana'   => 'anana-bay',
    'beast'    => 'east-bay',
    'dough'    => 'ough-day',
    'happy'    => 'appy-hay',
    'question' => 'estion-quay',
    'star'     => 'ar-stay',
    'three'    => 'ee-thray',
    'eagle'    => 'eagle-ay',
];