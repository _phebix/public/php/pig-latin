<?php
return [
    'banana'   => 'ananabay',
    'beast'    => 'eastbay',
    'dough'    => 'oughday',
    'happy'    => 'appyhay',
    'question' => 'estionquay',
    'star'     => 'arstay',
    'three'    => 'eethray',
    'eagle'    => 'eagleay',
];