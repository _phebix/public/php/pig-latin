<?php

use Tester\Environment;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/Phebix/PigLatinTranslator.php';

Environment::setup();

date_default_timezone_set('Europe/Prague');

define('TMP_DIR', __DIR__ . '/../tmp');
