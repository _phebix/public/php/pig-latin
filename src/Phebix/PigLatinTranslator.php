<?php
/**
 * Created by PhpStorm.
 * User: phebix
 * Date: 12.9.17
 * Time: 7:46
 */

namespace Phebix;

/**
 * Class PigLatinTranslator
 *
 * Simple class for translate English to Pig Latin
 *
 * @package Phebix
 * @see     https://en.wikipedia.org/wiki/Pig_Latin
 */
class PigLatinTranslator
{


    CONST VOWELS_LIST = ['a', 'e', 'i', 'o', 'u', 'y'];

    CONST VOWEL_SUFFIX_AY = 'ay';
    CONST VOWEL_SUFFIX_YAY = 'yay';
    CONST VOWEL_SUFFIX_WAY = 'way';
    CONST VOWEL_SUFFIX_HAY = 'hay';

    CONST CONSONANT_SUFFIX_A = 'a';
    CONST CONSONANT_SUFFIX_AY = 'ay';


    /**
     * @var string Selected vowel suffix
     */
    protected $vowelSuffix = self::VOWEL_SUFFIX_AY;


    /**
     * @var string Selected consonant suffix
     */
    protected $consonantSuffix = self::CONSONANT_SUFFIX_AY;


    /**
     * @var bool Is append hyphen
     */
    protected $useHyphen = false;

    //=========================================================================
    //=========================================================================
    //=========================================================================

    /**
     * PigLatinTranslator constructor.
     *
     * Options have available:
     * - vowelSuffix
     * - consonantSuffix
     * - useHyphen
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        if (!empty($options)) {
            if (key_exists('vowelSuffix', $options)) {
                $this->setVowelSuffix($options['vowelSuffix']);
            }
            if (key_exists('consonantSuffix', $options)) {
                $this->setConsonantSuffix($options['consonantSuffix']);
            }
            if (key_exists('useHyphen', $options)) {
                $this->setUseHyphen($options['useHyphen']);
            }
        }
    }

    //=========================================================================
    //=========================================================================
    //=========================================================================

    /**
     * Set Vowel suffix
     *
     * @param string $suffix
     * @return self
     * @throws \InvalidArgumentException
     */
    public function setVowelSuffix(string $suffix)
    {
        if ($suffix !== static::VOWEL_SUFFIX_AY
            && $suffix !== static::VOWEL_SUFFIX_YAY
            && $suffix !== static::VOWEL_SUFFIX_WAY
            && $suffix !== static::VOWEL_SUFFIX_HAY
        ) {
            throw new \InvalidArgumentException('Unsupported vowel suffix');
        }
        $this->vowelSuffix = $suffix;

        return $this;
    }
    //=========================================================================
    //=========================================================================
    //=========================================================================

    /**
     * Set consonant suffix
     *
     * @param string $suffix
     * @return self
     * @throws \InvalidArgumentException
     */
    public function setConsonantSuffix(string $suffix)
    {
        if ($suffix !== static::CONSONANT_SUFFIX_A && $suffix !== static::CONSONANT_SUFFIX_AY) {
            throw new \InvalidArgumentException('Unsupported consonant suffix');
        }
        $this->consonantSuffix = $suffix;

        return $this;
    }
    //=========================================================================
    //=========================================================================
    //=========================================================================

    /**
     * Set why use hyphen or not
     *
     * @param bool $value
     * @return self
     */
    public function setUseHyphen(bool $value)
    {
        $this->useHyphen = $value;

        return $this;
    }
    //=========================================================================
    //=========================================================================
    //=========================================================================

    /**
     * Translate string from English to Pig Latin
     *
     * @param string $text
     * @return string
     * @throws \InvalidArgumentException
     */
    public function translate(string $text): string
    {

        $out = '';
        foreach (preg_split('#\b|([[:punct:]])#', $text, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE) as $word) {
            $word = trim($word);

            if (empty($word)) {
                // any white char or punct
                $out .= ' ';
            } elseif (preg_match('#^[[:punct:]]$#', $word)) {
                //any punct char
                $out .= $word;
            } else {
                // any normal word

                $wordUppercase = preg_match('#^[A-Z]$#', $word);
                $firstUppercase = preg_match('#^[A-Z]$#', substr($word, 0, 1));

                $outWord = $this->translateWordToPigLatin(strtolower($word));

                if ($wordUppercase) {
                    // all chars upper => modify all to upper
                    $out .= strtoupper($outWord);
                } elseif ($firstUppercase) {
                    // first char upper => modify first to upper
                    $out .= ucfirst($outWord);
                } else {
                    // all lowe => no modify
                    $out .= $outWord;
                }
            }
        }

        return $out;
    }

    //=========================================================================
    //=========================================================================
    //=========================================================================
    /**
     * Translate English wort to Pig Latin
     *
     * @param string $word
     * @return string
     */
    protected function translateWordToPigLatin(string $word): string
    {

        $conCluster = $this->getWordConsonantCluster($word);
        $conClusterLen = strlen($conCluster);

        if ($conClusterLen === 0) {
            // start with vowel => add only vowelSuffix
            return $word . ($this->useHyphen ? '-' : '') . $this->vowelSuffix;
        } else {
            // no start with consonant) => move consonantCluster to end and add consonantSuffix
            return substr($word, $conClusterLen) . ($this->useHyphen ? '-' : '') . $conCluster . $this->consonantSuffix;
        }
    }

    //=========================================================================
    //=========================================================================
    //=========================================================================

    protected function getWordConsonantCluster(string $word)
    {
        $charsList = str_split($word);
        $cluster = '';
        $i = 0;
        foreach ($charsList as $char) {
            if ($i > 0 && $char === 'u' && $charsList[$i - 1] === 'q') {
                // current U and previous Q
                $cluster .= 'u';
            } elseif (in_array($char, static::VOWELS_LIST)) {
                // vowel => end of cluster
                return $cluster;
            } else {
                $cluster .= $char;
            }
            $i++;
        }
        return $cluster;
    }

    //=========================================================================
    //=========================================================================
    //=========================================================================
}